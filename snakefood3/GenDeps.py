import argparse
import ast
from collections import defaultdict
from pathlib import Path
import os
import sys
from typing import Generator, Set, Union
import fnmatch
from enum import IntFlag

from snakefood3.Graph import graph

def cleanseFilepath( filepath: str)-> str:
	# Op must return result not solely modify input
	ops= [ os.path.expanduser, os.path.abspath, os.path.expandvars]
	for op in ops:
		filepath= op( filepath)

	return filepath

class ModuleMembership( IntFlag):
	NONE= 0
	INTERNAL= 1
	EXTERNAL= 2
	BOTH= INTERNAL| EXTERNAL

class GenerateDependency:
	def __init__(
		self, rootPath: str, packageName: str, moduleMembership: ModuleMembership, excludeInclude: set[ str], grouping: set[ str], exclude: bool
	)-> None:
		self._rootPath= Path( rootPath)
		self._packageName= packageName
		self._packageGlob= packageName+ "*"
		self._moduleMembership= moduleMembership
		if grouping== None:
			self._grouping= set()
		else:
			self._grouping= grouping
		self._excludeInclude= excludeInclude
		self._exclude= exclude
		self._internalPackages= None

	@classmethod
	def iterPyFiles(
		cls, directoryPath: Union[ Path, str], extension= "py"
	)-> Generator[ Path, None, None]:
		"""Get all the files under a directory with specific extension

		:param directory_path: directory path
		:param extension: file extension, defaults to "py"
		:yield: Generator which yields path of all file of a specific extension
		"""
		yield from Path( directoryPath).rglob( f"*.{extension}")

	@classmethod
	def moduleToFilename( cls, moduleName: str, rootPath: Union[ Path, str])-> Path:
		"""Given a module name and root path of the module, give module path

		:param module_name: module name as in import statement (e.g. a.lib.csv_parser)
		:param root_path: root path of the module
		:return: path of the module
		"""
		return Path( rootPath)/ (moduleName.replace( ".", "/")+ ".py")

	@classmethod
	def filenameToModule(
		cls, filepath: Union[ Path, str], rootPath: Union[ Path, str]
	)-> str:
		"""Given a filepath and a root_path derive the module name as in import statement

		:param filepath: file path of the module
		:param root_path: root path of the package
		:return: module name as in import statement (e.g. a.lib.csv_parser)
		"""
		realpath= str( Path( filepath).relative_to( rootPath))
		realpath= realpath.replace( "\\", ".")
		realpath= realpath.replace( "/", ".")
		realpath= realpath.split( ".py")[ 0]
		if realpath.endswith( ".__init__"):
			realpath= realpath.split( ".__init__")[ 0]
		return realpath

	@classmethod
	def getFirstPrefixMatchingString( cls, string: str, *prefixes: str)-> str:
		"""Given prefixs and a string, return the first prefix with which the string starts. If no such prefix is present, return the original string

		:param string: string which will be matched against multiple prefix
		:return: first prefix with which the string starts if present, else return original string
		"""
		for prefix in prefixes:
			if string.startswith( prefix):
				return prefix
		return string

	def moduleMatchesPrescencePattern( self, module: str)-> bool:
		"""Enforce exclude/ Include prescence specifier rule"""
		# if self._exclude
		# 	for module
		# 		if module matches any pattern then it should not be included-> False
		# 		if module matches no patter then it should be included-> True

		# if not self._exclude
		# 	for module
		# 		if module matches any pattern then it should be included-> True
		# 		if module matches no pattern then it should not be included-> False

		# patternResult:
		# if module matches any pattern-> True
		# if module matches no pattern-> False

		# exclude-> True
		# True!= True-> False
		# True!= False-> True

		# exclude-> False
		# False!= True-> True
		# False!= False-> False
		if self._excludeInclude!= None:
			return self._exclude!= any( [ fnmatch.fnmatchcase( module, pattern) for pattern in self._excludeInclude])
		return True

	def importMatchesExclusion( self, module: str)-> bool:
		"""Enforce internalOnly and externalOnly rule"""
		if self._moduleMembership!= ModuleMembership.BOTH:
			result= fnmatch.fnmatchcase( module, self._packageGlob)
			if self._moduleMembership== ModuleMembership.EXTERNAL:
				result= not result
			return result
		return True

	def getInternalPackages( self)-> Set[ Path]:
		"""Get all the internal packages for a project"""
		if self._internalPackages is None:
			pythonPath= self._rootPath.resolve()
			self._internalPackages= {
				directory
				for directory in pythonPath.iterdir()
				if directory.is_dir() and ( directory/ "__init__.py").exists()
			}
		return self._internalPackages

	def _getAllImportsOfFile( self, filename: Path, currentModule: str)-> Set[ str]:
		"""Returns all imports of a file in module format"""
		if filename.name== "__init__.py":
			currentModule+= ".__init__"
		imports= set()

		if self._excludeInclude!= None:
			def addImportWithFilter( _import: str):
				nonlocal imports
				if self.moduleMatchesPrescencePattern( _import) and self.importMatchesExclusion( _import):
					imports.add( _import)
		else:
			def addImportWithFilter( _import: str):
				nonlocal imports
				imports.add( _import)
			
		# Matches all imports including those in function defs
		for node in ast.walk(
			ast.parse( source= filename.read_text( encoding="utf8"), filename= filename)
		):
			if isinstance( node, ast.Import):
				for name in node.names:
					addImportWithFilter( name.name)
			elif isinstance( node, ast.ImportFrom):
				# find which module is imported from
				# from .a import b # module=='a',name=='b'
				# maybe base_module.a.b or Base_module.a#b
				# from .a.b import c # module=='a.b',name=='c'
				# maybe base_module.a.b.c or Base_module.a.b#c
				# module should be
				if node.level== 0:
					module= node.module
				else:
					# If it is a relative import, level increases by 1 for every "." specified
					module= ".".join( currentModule.split( ".")[ : -node.level])
					if node.module:
						module+= "."+ node.module
					else:
						# from . import b # module==None, name=='b'
						# maybe base_module.b or Base_module#b
						pass

				for name in node.names:
					maybeDir= (
						self._rootPath/ "/".join( module.split( "."))/ name.name
					)
					maybeFile= self.moduleToFilename(
						module+ "."+ name.name, self._rootPath
					)
					if Path( maybeDir).exists() or Path( maybeFile).exists():
						addImportWithFilter( module+ "."+ name.name)
					else:
						addImportWithFilter( module)
		return imports

	def getImportMap( self)-> defaultdict[ str, Set[ str]]:
		"""Gets the import mapping for each module in the project"""
		imports= defaultdict( set)
		internalPackages= self.getInternalPackages()

		for file in self.iterPyFiles( self._rootPath/ self._packageName):

			# exclude/ Include glob behaviour is enforced for specified package component-s in the following function
			currentModule= self.filenameToModule( file, self._rootPath)
			if not self.moduleMatchesPrescencePattern( currentModule):
				continue

			# exclude/ Include glob behaviour is enforced for import-s in the following function
			# internal| external module import-s are enforced in the following function
			fileImports= self._getAllImportsOfFile( file, currentModule)

			imports[
				# Enforce grouping of specified package component-s
				self.getFirstPrefixMatchingString(
					currentModule, *self._grouping
				)
			].update(
				{
					# Enforce grouping of imports
					self.getFirstPrefixMatchingString(
						_import, *self._grouping
					)
					for _import in fileImports
					# Purpose uncertain
					if [ c for c in str( internalPackages) if _import.startswith( str( c))]
				}
			)

		formattedImports= defaultdict( set)
		for source, dist in imports.items():
			if dist:
				for d in dist:
					if source!= d:
						formattedImports[ source].add( d)
		return formattedImports

	def makeDotFile( self)-> str:
		"""Use existing template to generate a graph in dot language"""
		return graph( self.getImportMap())


def main()-> None:
	parser= argparse.ArgumentParser()
	parser.add_argument(
		"-i", "--internalOnly", help="the import-s of module-s within the specified package are only comprised of result-s from package-s internal to the specified package", action= "store_const", const= ModuleMembership.INTERNAL, default= ModuleMembership.NONE,
	)
	parser.add_argument(
		"-e", "--externalOnly", help="the import-s of module-s within the specified package are only comprised of result-s from package-s external to the specified package", action= "store_const", const= ModuleMembership.EXTERNAL, default= ModuleMembership.NONE,
	)
	parser.add_argument(
		"-g", "--grouping", help="a list of glob-s or a file containing a list of glob-s\nwhich module-s should have all contained package-s output as one accumulated output", nargs= "*"
	)
	parser.add_argument(
		"-f", "--excludeInclude", help="a list of glob-s or a file containing a list of glob-s\nwhich matched module-s and package-s should be excluded or included from the output\ndefault is include, behaviour is changed by argument -x", nargs= "*"
	)
	parser.add_argument(
		"-x", "--exclude", help="change the --excludeInclude parameter to be exclusionary as opposed to inclusionary", action= "store_true",
	)
	parser.add_argument("projectPath")
	parser.add_argument("packageName")

	args = parser.parse_args()

	moduleMembership= args.internalOnly| args.externalOnly
	if moduleMembership== ModuleMembership.NONE:
		moduleMembership= ModuleMembership.BOTH
	
	stdin= None

	moduleOptions= {}
	for moduleOption in { "grouping", "excludeInclude"}:
		value= getattr( args, moduleOption)
		if value== None:
			moduleOptions[ moduleOption]= None
			continue
		if len( value)== 0:
			print( f"Reading {moduleOption} value from stdin")
			if stdin== None:
				stdin= sys.stdin.read()
			moduleOptions[ moduleOption]= stdin
		file= False
		if len( value)== 1:
			file= True
			try:
				fp= cleanseFilepath( value[ 0])
				if not os.path.exists( fp):
					file= False
			except:
				file= False

		if file:
			with open( fp, "r") as file:
				value= { line.strip() for line in file.readlines() if line.strip()}
		else:
			value= set( value)
		moduleOptions[ moduleOption]= value
				
	print( GenerateDependency( args.projectPath, args.packageName, moduleMembership, **moduleOptions, exclude= args.exclude).makeDotFile())
